function Convert_Hours(seconds) {
	// body...
	return seconds * 60 * 60;
}

console.log(`2 hours = ${Convert_Hours(2)} seconds`);

console.log(`10 hours = ${Convert_Hours(10)} seconds`);

console.log(`24 hours = ${Convert_Hours(24)} seconds`);