# weekly-assement-4

## GIT QOESTION

```
 1. HOW TO WORKFLOW FOR GIT UNTIL IT'S UPDATED TO REPOSITORY ?

 2.  WHATS GIT ? WHAT'S GIT USE FOR ?

 3. HOW TO TRACK GIT PROGRESS ?
```
## GIT ANSWER

 1. workflow for git update to repository
	- install git
	- masuk ke gitlab
	- membuat project
	- memclone project
	- membuka hasil clone di text editor
	- melakukan git dasar di git bash
	- `git init`
	- `git status`
	- `git add .`
	- `git commit -m "..."`
	- `git checkout -b feature_branch`
	- `git push remote main`
#
 2. what git ?
 ``
 Git adalah salah satu sistem pengontrol versi (Version Control System) pada proyek perangkat lunak yang diciptakan oleh Linus Torvalds. Pengontrol versi bertugas mencatat setiap perubahan pada file proyek yang dikerjakan oleh banyak orang maupun sendiri
 ``
#
 3. how track git progress ?
 ``
 track git progress dengan `git status` di terminal
 ``
